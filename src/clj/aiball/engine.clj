(ns aiball.engine
  (:import (clojure.lang PersistentVector))
  (:use org.httpkit.server)
  (:use org.httpkit.timer)
  (:use overtone.at-at)
  (:require [clojure.data.json :as json])
  (:require [org.httpkit.timer :as timer])
  (:require [clojure.test :refer :all])
  (:require [clojure.tools.logging :as log]))

(defonce server (atom nil))
(defonce game-state (atom []))
(defonce channel-uid (atom {}))
(defonce scheduler (atom  []))
(defonce commands (atom {}))
(defonce rules (atom []))

(set! *warn-on-reflection* true)

(declare create-player interaction-rules change)

(def tact-ms 15)
;(def tact (/ 1000 tact-period))
(def tact-sec (/ tact-ms 1000))
(def send-period-ms 15)
(def rocket-velocity 2000)
(def rocket-accuracy 200)
(def lava-damage 20)
(def c {:player    {:v-max 500 :ctrl-acc 1500 :radius 20 :mass 1 :can-shoot true}
        :spectator {:v-max 1000 :ctrl-acc 1500 :radius 10 :mass 0.1}
        :rocket    {:v-max 4000 :ctrl-acc 8000 :radius 7 :mass 0.3}
        :explosion {:v-max 0 :ctrl-acc 0 :radius 150 :visibility-duration 1500}})
(def field-params {:width 1500, :height 800, :shooting-cooldown 300})

(defn stop-server []
  (when-not (nil? @server)
    ;; graceful shutdown: wait 100ms for existing requests to be finished
    ;; :timeout is optional, when no timeout, stop immediately
    (map close (keys @channel-uid))
    (reset! channel-uid {})
    (@server :timeout 100)
    (reset! server nil)))

(defn change [key fun & args]
  (fn [state]
    (apply update-in state [key] fun args)))

(defn change-once [cond-flag key fun & args]
  "you will need to clean cond-flag every tact using a different update function with less priority"
  (fn [state]
    (if (cond-flag state)
      state
      (assoc (apply update-in state [key] fun args) cond-flag true))))

(defn change-if [cond-flag key fun & args]
  (fn [state]
    (if (cond-flag state)
      (apply update-in state [key] fun args)
      state)))

(defn m+ [& ms]
  (apply mapv + ms))

(defn m- [& ms]
  (apply mapv - ms))

(defn m* [& ms]
  (let [{scalars false vecs true} (group-by sequential? ms)
        scalar (apply * scalars)]
    (mapv (partial * scalar)
          (apply mapv * vecs))))

(defn dot* [m1 m2]
  (apply + (mapv * m1 m2)))

(defn len [m]
  (Math/sqrt (apply + (map #(* % %) m))))

(defn direction [m]
  (let [L (len m)]
    (if (zero? L)
      (vec (map (constantly 0) m))
      (m* m (/ 1 (len m))))))

(defn eq?
  "Approx equality for floats (eps = 1e-6)"
  [& fs]
  (> 1e-6 (Math/abs ^Double (- (apply max fs) (apply min fs)))))

(defn ahead? [pos v target]
  (pos? (dot* (m- target pos) v)))

(defn projection [A B]
  "Project of vector A on B"
  (m* B (/ (dot* A B)
           (dot* B B))))

(deftest matrix-tests
  (is (true? (eq? 0.00000001 0 0.0)))
  (is (true? (eq? 0.99999999 1)))
  (is (false? (eq? 0.0001 0 0.0)))
  (is (false? (eq? 2 3)))
  (is (== 1 (len [1 0])))
  (is (= [4 7] (m+ [1 2] [3 5])))
  (is (= [6 3] (m* [2 1] 3)))
  (is (eq? 1 (len (direction [123 -454324]))))
  (is (= [0 0] (direction [0 0])))
  (is (true? (ahead? [2 3] [1 1] [3 4])))
  )

(defn interaction [{t1 :type :as o1} {t2 :type :as o2}]
  (let [fns-12 (filter (fn [[subjects objects cond _ _]] (and (subjects t1) (objects t2) (cond o1 o2))) @rules)
        fns-21 (filter (fn [[subjects objects cond _ _]] (and (subjects t2) (objects t1) (cond o2 o1))) @rules)]
    {o2 (map (fn [[_ _ _ w func]] [w (func o1 o2)]) fns-12)
     o1 (map (fn [[_ _ _ w func]] [w (func o2 o1)]) fns-21)}))

(defn move [[{v0 :v} {v1 :v pos :pos :as new-obj}]]
  (if v1
    (let [V-mean (m* 0.5 (m+ (or v0 v1) v1))
          delta-pos (m* V-mean tact-sec)
          new-pos (m+ pos delta-pos)]
      (assoc new-obj :pos new-pos))
    new-obj))

(defn sort-by-order [w-f]
  "Less number is first"
  (apply concat (map second (sort-by first w-f))))

(defn apply-changes [initial-object funcs]
  "Returns tuple of initial object and a new object"
  (loop [obj initial-object
         [f & fs] funcs
         spawned-objects ()]
    (if (and obj f)
      (let [res (f obj)
            [modified-obj & local-spawned] (if (sequential? res)
                                             res
                                             [res])]
        (recur modified-obj fs (concat local-spawned spawned-objects)))
      (conj (mapv #(vector nil %) spawned-objects) [initial-object obj]))))                      ;; TODO: rewrite this whole clusterfuck

(is (= (apply-changes {:1 1 :2 2} [#(update-in % [:1] + 3)]) [[{:1 1 :2 2} {:1 4 :2 2}]]))
(is (= (apply-changes {:1 1 :2 2} [#(vector % 1) (constantly nil) #(update-in % [:1] + 3)]) [[nil 1] [{:1 1, :2 2} nil]]))

(defn update-state [gm-state]
  (let [obj-with-effects (apply merge-with concat
                                (first (reduce (fn [[res objs] o]
                                                 [(concat res (mapv (partial interaction o) objs))
                                                  (conj objs o)])
                                               [() []] gm-state)))
        objs-old-and-new (apply concat (for [obj gm-state
                                             :let [funcs (sort-by-order (get obj-with-effects obj))]]
                                         (apply-changes obj funcs)))
        moved-objs (map move objs-old-and-new)]
    (filter (comp not nil?) moved-objs)
    ))

(defn create-player [{:keys [uid pos look-at] :or {pos [(rand-int (:width field-params))
                                                        (rand-int (:height field-params))]
                                                   look-at [0 0]}}]
  {:type            :player
   :radius          (:radius (:player c))
   :uid             uid
   :pos             pos
   :time-till-shoot 0
   :health          100
   :mass            (:mass (:player c))
   :v               [0 0]
   :g               [0 0]
   :look-at         look-at})

(defn add-player
  "Add a new player to game"
  [state uid]
  ;(println state uid)
  (conj state (create-player {:uid uid})))

(defn remove-player [state uid]
  "(state, uid) -> state"
  (if uid
    (filter #(not (nil? %)) (for [obj state]
                              (if (= uid (:uid obj))
                                nil
                                (if (= :frag-table (:type obj))
                                  (update-in obj [:table] dissoc uid)
                                  obj))))
    state))

(defn init-player! [channel data-map]
  (swap! channel-uid assoc channel (data-map "uid"))
  (swap! game-state add-player (data-map "uid"))
  (send! channel (json/write-str field-params)))

(defn handler [req]
  (with-channel req channel

                (on-close channel (fn [status]
                                    (swap! game-state remove-player (@channel-uid channel))
                                    (swap! channel-uid dissoc channel)))
                (on-receive channel (fn [data]
                                      (let [data-map (json/read-str data)]
                                        (case (data-map "command")
                                          "connect" (init-player! channel data-map)
                                          "input" (swap! commands merge {(data-map "uid") data-map})
                                          nil))
                                      ;(send! channel (json/write-str @game-state))
                                      ))))

(defn has-type? [state tp]
  (< 0 (count (filter #(= (:type %) tp) state))))

(defn update-commands [game-state commands]
  (for [s game-state]
    (if (= :commands (:type s))
      (assoc commands :type :commands)
      s)))

(defn advance-state! []
  (swap! game-state update-commands @commands)
  (swap! game-state update-state)
  )

(defn send-state-to-all []
  (let [json-state (json/write-str @game-state)]
    (doseq [c (keys @channel-uid)]
      (send! c json-state))))

(defn init-connetions-timer []
  (every send-period-ms send-state-to-all (mk-pool)))

(defn init-update-timer []
  (every tact-ms advance-state! (mk-pool)))

(defn -main [& args]
  (reset! game-state [{:type :commands}
                      {:type :nature}
                      {:type :frag-table :table {}}
                      {:type :island
                       :pos [500 400]
                       :radius 390}
                      {:type :island
                       :pos [900 -400]
                       :radius 890}
                      {:type :island
                       :pos [1000  400]
                       :radius 200}
                      {:type :island
                       :pos [1200  1000]
                       :radius 60}
                      {:type :island
                       :pos [1000  1400]
                       :radius 60}])
  (reset! commands {})
  (reset! rules [])

  (reset! server (run-server #'handler {:port 8080}))
  (reset! scheduler [(init-update-timer) (init-connetions-timer)
                     ]))

(defn restart []
  (doseq [s @scheduler]
    (kill s))
  (stop-server)
  (-main))

(defn in?
  "Tests if element in a collection"
  [coll element]
  (true? (some #(= element %) coll)))

(is (true? (in? [1 2 3] 2)))
(is (false? (in? [1 2 3] nil)))

(defn get-fn-arity [f]
  (let [^java.lang.reflect.Method m (first (.getDeclaredMethods (class f)))
        p (.getParameterTypes m)]
    (alength p)))

(defn add-rule! [subj obj cond func order]
  (let [f (if (= 1 (get-fn-arity func))
            (constantly [func])
            func)]
    (swap! rules conj [subj obj cond order f])))

(defn split-by [pred coll]
  "Split coll into two parts
  >>> (split-by pos? [-1 -2 -3 4 -5 -6])
  [(-1 -2 -3) (-5 -6)]"
  (loop [[x & xs] coll
         a ()]
    (if (nil? x)
      ([a ()])
      (if (pred x)
        [a xs]
        (recur xs (conj a x))))))

(is (= ['(:player :rocket) '(:rocket)]
       (split-by #(= % '->) '(:rocket :player -> :rocket))))

(defn params-map [params-list]
  (let [keys (butlast params-list)
        values (rest params-list)
        pairs (mapv vector keys values)
        valid-pairs (filter #(keyword? (first %)) pairs)]
    (into {} valid-pairs)))

(is (= {:when :no-health-left,
        :order 5,
        :no-health-left :order} (params-map [:when :no-health-left :order 5 pos?])))

(defmacro defrule [xbjs func & params-list]
  (let [[subjs objs] (if (in? xbjs '->)
                       (split-by #(= % '->) xbjs)
                       [[:nature] xbjs])
        params (params-map params-list)
        order (get params :order 0)
        when (get params :when (fn [_ _] true))]
    (list 'add-rule! (set subjs) (set objs) when func order)))

(defmacro
  ^{
     :arglists '([^String description ^PersistentVector commons & rules]
                 [^PersistentVector common & rules])
     :doc "Create muliple rules with common parameters and common description"}
  defrules
  [p1 & ps]
  (vec (if (string? p1)
         (let [msg p1
               [commons & rules] ps]
           (for [p rules]
             (list* 'defrule (concat p commons))))      ; TODO Marat: send msg to defrule
         (let [commons p1
               rules ps]
           (for [p rules]
             (list* 'defrule (concat p commons)))))))