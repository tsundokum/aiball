(ns aiball.core
  (:use aiball.engine)
  (:use clojure.repl)
  (:use clojure.pprint))

(defn neighbor? [o1 o2]
                         (and (not= o1 o2)) (< (len (m- (:pos o1) (:pos o2))) (+ (:radius o1) (:radius o2))))

(defn explode [{pos :pos owner :owner}]
  (let [radius (:radius (:explosion c))
        life-span (:visibility-duration (:explosion c))]
    {:type       :explosion
     :radius     radius
     :pos        pos
     :v          [0 0]
     :g          [0 0]
     :active     true
     :life-span  life-span
     :visible-ms life-span
     :owner      owner}))


(defn player-death [pstate]
  (assoc pstate :type :spectator :radius (:radius (:spectator c)) :mass (:mass (:spectator c)) :health 100))

(defn explosion-controls [{visible-ms :visible-ms :as estate}]
  (if (pos? visible-ms)
    (update-in estate [:visible-ms] #(- % tact-ms))
    nil))

(defn rocket-controls [_ {pos :pos T :target S :start-pos h :health v :v :as rstate}]
  (if (ahead? pos (m- T S) T)
    (let [Direction (direction (m- T pos))
          G (m* Direction (:ctrl-acc (:rocket c)))]
      [#(assoc % :g G)])
    [#(assoc % :health 0 :v (m- [0 0] v) :g [0 0])]))

(defn spawn-rocket [{pos :pos uid :uid V :v pradius :radius} target]
  (let [target-direction (direction (m- target pos))
        G (m* target-direction (:ctrl-acc (:rocket c)))]
    {:type      :rocket
     :radius    (:radius (:rocket c))
     :start-pos pos
     :pos       (m+ pos (m* target-direction 1.5 pradius))
     :health    10
     :v         V
     :g         G
     :mass      (:mass (:rocket c))
     :target    target
     :owner     uid}))

(defn apply-explosion [{active :active damager :owner :as ex-state} obj-state]
  (if active
    (let [push-vector (m- (:pos obj-state) (:pos ex-state))
          push-direction (direction push-vector)
          push-force (/ 1 (len push-vector))]
      ;(println push-force)
      [(change :v m+ (m* push-direction push-force 90000))
       (change :health - (* 2000 push-force))
       #(assoc % :last-damager damager)])))

(defn calc-friction [V v-max g]
  (if (== 0 v-max) [0 0] (m* -1 g V (/ 1 v-max))))

(defn apply-acc [_ {tp :type V0 :v G :g}]
  "Return change in velocity given its control G"
  (let [v-max (:v-max (tp c))
        A (m+ G (calc-friction V0 v-max (:ctrl-acc (tp c))))
        V-new (m+ V0 (m* tact-sec A))
        ]
    [(change :v m+ (m- V-new V0))]))

(defn collide [{spos :pos sv :v sm :mass :as subj} {opos :pos ov :v om :mass :as obj}]
  (let [base (m- opos spos)
        based-sv (m* (projection sv base) sm (/ 1 om))
        based-ov (projection ov base)]
    [(change :v m+ (m- (if (ahead? spos based-sv opos) based-sv [0 0])
                       (if (ahead? opos based-ov spos) based-ov [0 0])))]))

(defn process-shooting [commands {uid :uid target :look-at :as pstate}]
  (let
      [{{shooting "shoot"} "input"} (commands uid)]
    (if (and shooting
             (< (:time-till-shoot pstate) 1))
      [(fn [obj] [(assoc obj :time-till-shoot (:shooting-cooldown field-params))
                  (spawn-rocket obj target)])]
      [(fn [obj] (update-in obj [:time-till-shoot] #(max 0 (- % tact-ms))))])))

(defn player-controls [commands {uid :uid :as pstate}]
  (let [{{f "forward" b "backward" l "left" r "right" look-at "look-at" target "shoot-at"} "input"} (commands uid)
        dy (+ (if f -1 0) (if b 1 0))
        dx (+ (if l -1 0) (if r 1 0))
        G (m* (:ctrl-acc (:player c)) (direction [dx dy]))]
    [#(assoc % :g G :look-at look-at)])
  )

(defn update-pos [{V0 :v pos :pos :as state} dVs]
  (let [V-mean (m* 0.5 (apply m+ V0 V0 dVs))
        new-pos (m+ pos (m* V-mean tact-sec))]
    (assoc state :v (apply m+ V0 dVs) :pos new-pos)))

(defn player-death [{killer :last-damager uuid :uid :as s}]
  [(-> s
       (assoc :type :spectator
                    :radius (:radius (:spectator c))
                    :mass (:mass (:spectator c))
                    :health 100)
       (dissoc :last-damager))
   {:type :frag
    :killer (or killer uuid)
    :victim uuid}])

(defn spectator-death [{should-respawn :island-healing :as s}]
  (if should-respawn
    (create-player s)
    s))

(defn rocket-death [{h :health :as s}]
  (explode s))

(defn dead? [_ {h :health}]
  (not (pos? h)))


(restart)

(defrules [:when neighbor?]
          ([:explosion -> :player :rocket] apply-explosion)
          ([:player :spectator -> :player :spectator] collide)
          ;([:rocket -> :player] collide)
          ([:player :rocket -> :rocket] (change :health - 30)))

(defrule [:commands -> :player :spectator] player-controls :order -1)
(defrule [:commands -> :player] process-shooting)

(defrule [:explosion] (change :active (constantly false)))
(defrule [:player :rocket :spectator] apply-acc)
(defrule [:rocket] rocket-controls :order 50)
(defrule [:explosion] explosion-controls :order 50)

(defrules "damage player at lava" []
          ([:player :spectator] (change :health - (* tact-sec lava-damage)))
          ([:island -> :player :spectator] #(assoc % :island-healing true) :order 0 :when neighbor?)
          ([:player] (change-if :island-healing :health + (* tact-sec lava-damage)) :order 1)
          ([:spectator] spectator-death :order 2 :when dead?)
          ([:player :spectator] #(dissoc % :island-healing) :order 3))

(defrules [:order 100 :when dead?]
          ([:player] player-death)
          ([:rocket] rocket-death))

(defn account-frag [{killer :killer victim :victim} frag-table]
  [(fn [ft]
     (-> ft
         (update-in [:table killer] #(let [[k d] (or % [0 0])]
                                      [((if (= killer victim) dec inc) k) d]))
         (update-in [:table victim] #(let [[k d] (or % [0 0])]
                                      [k (inc d)]))))])

(defrule [:frag -> :frag-table] account-frag)
(defrule [:frag] (fn [_] nil))