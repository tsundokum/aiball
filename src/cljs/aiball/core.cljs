(ns aiball.core
  (:require
    [goog.string.StringBuffer]))

(declare uid set-transform!)

(defn log [& msgs]
  (apply #(.log js/console %) msgs))

(def tick-period 15)
(def shooting-cooldown 2000)

(defn m+ [& ms]
  (apply mapv + ms))

(defn m- [& ms]
  (apply mapv - ms))

(defn m* [& ms]
  (let [{scalars false vecs true} (group-by sequential? ms)
        scalar (apply * scalars)]
    (mapv (partial * scalar)
          (apply mapv * vecs))))

(defn dot* [m1 m2]
  (apply + (mapv * m1 m2)))

(def href (let [host (.-host (.-location js/window))]
            (str "ws://" (if (empty? host) "localhost" host) ":8080/")))

(def canvas (.createElement js/document "canvas"))
(def ctx (.getContext canvas "2d"))
(def lava-texture (js/Image.))
(def grass-texture (js/Image.))
(set! (.-src lava-texture) "http://image.bolterandchainsword.com/uploads/gallery/album_9069/gallery_64782_9069_14474.jpg")
(set! (.-src grass-texture) "http://imgur.com/1IeJZ.jpg")
(def conn (js/WebSocket. href))

(def field {:x 600
            :y 600
            :transform-matrix {:offset [0 0]
                               :scale  [1 1]}})

(def input-state (atom {:forward  false
                        :backward false
                        :left     false
                        :right    false
                        :stats    false
                        :look-at  [(/ (:x field) 2)
                                   (/ (:y field) 2)]}))

(def time-to-redraw true)

(defn clear-canvas! [{[offset-x offset-y] :offset [scale-x scale-y] :scale}]
  (set! (.-fillStyle ctx) (.createPattern ctx lava-texture "repeat"))
  (.fillRect ctx
             (/ (* (Math/ceil (/ offset-x (:x field))) (- (:x field))) scale-x)
             (/ (* (Math/ceil (/ offset-y (:y field))) (- (:y field))) scale-y)
             (* (/ 2 scale-x) (:x field)) (* (/ 2 scale-y) (:y field))))

(defn draw-player! [{uid "uid" :as player-state}]
  (let [{[x y] "pos" [x1 y1] "look-at" radius "radius" time-till-shoot "time-till-shoot" health "health"} player-state
        color (str "#" (subs uid 0 6))
        vector-length (.sqrt js/Math (+ (* (- x1 x) (- x1 x)) (* (- y1 y) (- y1 y))))
        time-color (apply str "#" (take 3 (repeat (.toString (int (* 255 (/ (max 0 time-till-shoot) shooting-cooldown))) 16))))]
    (set! (.-fillStyle ctx) color)
    (.beginPath ctx)
    (.arc ctx x, y, radius, 0, (* 2 (.-PI js/Math)))
    (.fill ctx)

    (set! (.-strokeStyle ctx) color)
    (set! (.-lineWidth ctx) (/ radius 1.5))
    (.beginPath ctx)
    ;(set! (.-fillStyle ctx) (str "#fffff"))
    (.moveTo ctx x y)
    (.lineTo ctx (+ x (* 1.7 radius (/ (- x1 x) vector-length))) (+ y (* 1.7 radius (/ (- y1 y) vector-length))))
    (.stroke ctx)

    ; healt-bar
    (set! (.-strokeStyle ctx) "#AA0000")
    (set! (.-lineWidth ctx) (/ radius 1.5))
    (.beginPath ctx)
    (.moveTo ctx (- x radius) (+ y (* 2 radius)))
    (.lineTo ctx (+ (- x radius) (* radius 2 (/ health 100))) (+ y (* 2 radius)))
    (.stroke ctx)

    ;reload indicator
    (set! (.-fillStyle ctx) time-color)
    (.beginPath ctx)
    (.arc ctx x, y, (/ radius 2), 0, (* 2 (.-PI js/Math)))
    (.fill ctx)))

(defn draw-spectator! [{uid "uid" :as player-state}]
  (let [{[xf yf] "pos" radius "radius" health "health"} player-state
        x (.round js/Math xf)
        y (.round js/Math yf)
        color (str "#" (subs uid 0 6))]

    (set! (.-fillStyle ctx) color)
    (.beginPath ctx)
    (.arc ctx x, y, radius, 0, (* 2 (.-PI js/Math)))
    (.fill ctx)

    ; healt-bar
    (set! (.-strokeStyle ctx) "#AA0000")
    (set! (.-lineWidth ctx) (/ radius 1.5))
    (.beginPath ctx)
    (.moveTo ctx (- x radius) (+ y (* 2 radius)))
    (.lineTo ctx (+ (- x radius) (* radius 2 (/ (max 0 health) 100))) (+ y (* 2 radius)))
    (.stroke ctx)))

(defn draw-rocket! [{uid "owner" [xf yf] "pos" radius "radius"}]
  (let [x (.round js/Math xf)
        y (.round js/Math yf)
        color (str "#" (subs uid 0 6))]
    (set! (.-fillStyle ctx) color)
    (.beginPath ctx)
    (.arc ctx x, y, radius, 0, (* 2 (.-PI js/Math)))
    (.fill ctx)))

(defn draw-island! [{uid "owner" [xf yf] "pos" radius "radius"}]
  (let [x (.round js/Math xf)
        y (.round js/Math yf)
        color "#009933"]
    ;(set! (.-fillStyle ctx) color)
    (set! (.-fillStyle ctx) (.createPattern ctx grass-texture "repeat"))
    (.beginPath ctx)
    (.arc ctx x, y, radius, 0, (* 2 (.-PI js/Math)))
    (.fill ctx)))

(defn draw-explosion! [{uid "owner" [xf yf] "pos" radius "radius" life-span "life-span" left-to-live "visible-ms"}]
  (let [x (.round js/Math xf)
        y (.round js/Math yf)
        color (str (subs uid 0 6))]
    (let [gradient-inner-radius 1                           ;(/ radius 5)
          percentage-lived (/ left-to-live life-span)
          grd (.createRadialGradient ctx x y gradient-inner-radius x y radius)
          rgb (apply str (interpose "," (mapv #(js/parseInt (apply str %) 16) (partition 2 color))))
          rgba-base #(str "rgba(" rgb "," % ")")
          rgba-inner (rgba-base percentage-lived)
          rgba-outer (rgba-base 0)]
      (.addColorStop grd 0 rgba-inner)
      (.addColorStop grd 1 rgba-outer)
      (set! (.-fillStyle ctx) grd))
    (.beginPath ctx)
    (.arc ctx x, y, radius, 0, (* 2 (.-PI js/Math)))
    (.fill ctx)))

(defn draw-stat-table! [{table "table"}]
  (.setTransform ctx 1 0 0 1 0 0)
  (set! (.-globalAlpha ctx) 0.7)

  (let [{c-width :x c-height :y} field
        ]
    (loop [[[player [k d]] & rest] (reverse (sort-by fnext (seq table)))
           i 0]
      (when player
        (set! (.-fillStyle ctx) (str "#" (subs player 0 6)))
        (set! (.-font ctx) "bold 32px Arial")
        (.fillText ctx (str k "/" d) 500 (+ 70 (* i 45)))
        (recur rest (inc i)))))

  (set-transform!)
  (set! (.-globalAlpha ctx) 1))


(def curr-state nil)

(def draw-order {"island" 0
                 "spectator" 1
                 "player" 3
                 "rocket" 4
                 "explosion" 20
                 "frag-table" 1000})

(def counter 0)
(def fps 0)

(defn set-transform! []
  (let [{[offset-x offset-y] :offset
         [scale-x scale-y] :scale} (:transform-matrix field)]
    (.setTransform ctx scale-x 0 0 scale-y offset-x offset-y)))

(defn get-player [state]
  "Get player object"
  (first (filter (fn [{puid "uid"}] (= puid uid))
                 state)))

(def old-pos [0 0])

(defn assign-view-point! [screen state]
  (let [{pos "pos" mouse "look-at" :as owner} (get-player state)
        scale (:scale (:transform-matrix field))
        center  (m* 0.5 screen)
        delta-pos (m- pos old-pos)
        look-at (if (empty? mouse) pos (m+ mouse delta-pos))
        view-center (m* (m+ (m* 3 pos) look-at) 0.25)
        offset (m- center (m* scale view-center))]
    (set! old-pos pos)
    (set! field (update-in field [:transform-matrix :offset] (constantly offset)))
    (set-transform!)))

(defn redraw [state]
  (set! counter (inc counter))
  (assign-view-point! [(:x field) (:y field)] state)
  (clear-canvas! (:transform-matrix field))
  (doseq [p (sort-by #(draw-order (% "type")) state)]
    (case (p "type")
      "island" (draw-island! p)
      "player" (draw-player! p)
      "spectator" (draw-spectator! p)
      "rocket" (draw-rocket! p)
      "explosion" (draw-explosion! p)
      "frag-table" (when (or
                           (= "spectator" ((get-player state) "type"))
                           (:stats @input-state)) (draw-stat-table! p))
      :hui))
  (.setTransform ctx 1 0 0 1 0 0)
  (set! (.-fillStyle ctx) "blue")
  (set! (.-font ctx) "bold 48px Arial")
  (.fillText ctx fps 30 50))

(js/setInterval (fn [] (set! fps counter) (set! counter 0)) 1000)

(defn react-on-state! [msg]
  (when time-to-redraw
    (redraw (js->clj (.parse js/JSON (.-data msg))))
    (set! time-to-redraw false))
  #_(set! curr-state (js->clj (.parse js/JSON (.-data msg)))))

(defn make-uuid []
  (letfn [(f [] (.toString (rand-int 16) 16))
          (g [] (.toString  (bit-or 0x8 (bit-and 0x3 (rand-int 15))) 16))
          (- [] "-")
          (m4 [] "-4")]
    (apply str ((juxt f f f f f f f f - f f f f m4 f f f - g f f f - f f f f f f f f f f f f)))))
(def uid (make-uuid))

;(def interval-count (atom 0))
;(def animation-count (atom 0))

(defn fix-mouse-coordinates [xy]
  (let [rect (.getBoundingClientRect canvas)
        {offset :offset scale :scale} (:transform-matrix field)
        screen [(.-left rect) (.-top rect)]]
    (m* (map (fn [a] (/ 1 a)) scale) (m- xy screen offset))))

(defn fix-input-mouse-coordinates [input]
  (update-in input [:look-at] fix-mouse-coordinates))

(defn ontick []
  (.send conn (.stringify js/JSON (js-obj "command" "input", "uid" uid, "input" (clj->js (fix-input-mouse-coordinates @input-state)))))
  (set! time-to-redraw true))

(defn onanimate [timestamp]
  ;(swap! animation-count inc)
  (ontick)
  (.requestAnimationFrame js/window onanimate))

(defn resize-canvas []
  (let [w (* 0.999 (.-innerWidth js/window))
        h (* 0.996 (.-innerHeight js/window))]
    (set! (.-width canvas) w)
    (set! (.-height canvas) h)
    (set! field (assoc field :x w :y h))))

(defn on-initialize [msg]
  (let [{w "width" h "height" sc "shooting-cooldown"} (js->clj (.parse js/JSON (.-data msg)))]
    (set! shooting-cooldown sc)
    (resize-canvas)
    (set! (.-id canvas) "gameCanvas")
    (.appendChild (.-body js/document) canvas)
    (.focus canvas))
  (set! (.-onmessage conn) react-on-state!)
  (.requestAnimationFrame js/window onanimate)
  ;(js/setInterval ontick tick-period)
  )

(set! (.-onopen conn)
      (fn [e]
        (.send conn
               (.stringify js/JSON (js-obj "command" "connect" "uid" uid)))
        (set! (.-onmessage conn) on-initialize)))

(defn user-input [cmd-name kwd-funs]
  (fn [event]
    (let [args (mapcat (fn [[k v]] [k (v event)]) kwd-funs)
          json (apply js-obj "command" cmd-name "uid" uid args)]
      (.send conn (.stringify js/JSON json)))))

(set! (.-onmessage conn)
      react-on-state!)

(defn onmousemove [event]
  (let [[x y] [(.-clientX event) (.-clientY event)]]
    (swap! input-state assoc :look-at [x y])))

(defn onmousedown [event]
  (onmousemove event)
  (swap! input-state assoc :shoot true))

(defn onmouseup [event]
  (swap! input-state assoc :shoot false))

(def key-codes {87 :forward, 83 :backward, 65 :left, 68 :right,
                38 :forward, 40 :backward, 37 :left, 39 :right,
                9 :stats})

(def ignored-keys #{9})                                     ;forbid default actions on some keys like Tab

(defn onkeydown [event]
  (let [k (.-keyCode event)]
    (if-let [action (key-codes k)]
      (swap! input-state assoc action true))
    (when (contains? ignored-keys k)
      (.preventDefault event)
      false)))

(defn onkeyup [event]
  (let [k (.-keyCode event)]
    (if-let [action (key-codes k)]
      (swap! input-state assoc action false))))

(defn onwheel [event]
  (let [delta-y (.-deltaY event)
        wheel-forward (> 0 delta-y)
        wheel-backward (< 0 delta-y)
        scale-modifier (if wheel-forward (/ 1 0.9)
                                         (if wheel-backward 0.9
                                                            1))]
    (set! field (update-in field [:transform-matrix :scale] #(m* % scale-modifier)))))

(set! (.-onmousemove js/window) onmousemove)
(set! (.-onmousedown js/window) onmousedown)
(set! (.-onmouseup js/window) onmouseup)
(set! (.-oncontextmenu js/window) (constantly false))
(set! (.-onresize js/window) (fn [_] (resize-canvas)))

(set! (.-tabIndex canvas) 1000)
(.addEventListener canvas "blur" (fn [_] (.focus canvas)) true) ;force canvas to be focused on

(.addEventListener canvas "keydown" onkeydown true)
(.addEventListener canvas "keyup" onkeyup true)
(.addEventListener js/window "wheel" onwheel true)


(defn pinch [event]
  (let [scale-modifier (.-scale event)]
    (set! field (update-in field [:transform-matrix :scale] #(m* % scale-modifier))))
  (.preventDefault event)
  false)

(.addEventListener js/window "touchstart" onmousedown true)
(.addEventListener js/window "touchmove" onmousemove true)
(.addEventListener js/window "touchend" onmouseup true)

;(.write js/document "<h2>Hello Browser</h2>")